QUICK START GUIDE
-----------------
1 Download the files from the addon:
  https://github.com/trentrichardson/jQuery-Timepicker-Addon
  Put them in the libraries/all/jquery-ui-timepicker/ folder

2 Enable the module

3 add the class 'timepicker' to a inputfield, and 
  the time will be pickables.


ADVANCED HELP
-------------
for custom datetimeformats and bugs, 
check http://trentrichardson.com/examples/timepicker/
